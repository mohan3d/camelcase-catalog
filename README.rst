camelCase Workshop
==================

A google-app-engine based catalog app consists of categories and products.

`Deployed version`_


Deployment
----------

.. code:: bash

    $ gcloud app deploy

**Note**: `gcloud`_ must be installed and configured.


Development server
------------------

.. code:: bash

    $ cd cd camelcase-catalog
    $ dev_appserver.py .


.. _gcloud: https://cloud.google.com/sdk/gcloud/
.. _Deployed version: https://camelcase-catalog.appspot.com/