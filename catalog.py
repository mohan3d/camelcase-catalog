#!/usr/bin/env python

import logging
import os

import jinja2
import webapp2
from webapp2_extras import routes

from models import get_default_key, Category, Product

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), 'templates')),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)


class BaseHandler(webapp2.RequestHandler):
    """Adds render template feature to children."""

    def render(self, template, **kwargs):
        template = JINJA_ENVIRONMENT.get_template(template)
        self.response.write(template.render(**kwargs))


class MainHandler(BaseHandler):
    def get(self):
        categories = Category.query(ancestor=get_default_key())
        self.render('home.html', categories=categories)


# ----------------- Category Handlers -----------------
class CategoryCreateHandler(BaseHandler):
    def get(self):
        self.render('category_new.html')

    def post(self):
        try:
            name = self.request.get('name')
            description = self.request.get('description')

            category = Category(parent=get_default_key(),
                                name=name,
                                description=description)
            category.put()

            self.redirect('/category/{}'.format(category.urlsafe))
        except Exception as e:
            logging.error(e.message)
            self.abort(404)


class CategoryRetrieveHandler(BaseHandler):
    def get(self, urlsafe):
        try:
            category = Category.get_by_key(urlsafe)
            self.render('category_detail.html', category=category)
        except Exception as e:
            logging.error(e.message)
            self.abort(404)


class CategoryUpdateHandler(BaseHandler):
    def get(self, urlsafe):
        category = Category.get_by_key(urlsafe)
        self.render('category_update.html', category=category)

    def post(self, urlsafe):
        name = self.request.get('name')
        description = self.request.get('description')

        try:
            Category.update_by_key(urlsafe,
                                   name=name,
                                   description=description)
            self.redirect('/category/{}'.format(urlsafe))
        except Exception as e:
            logging.error(e.message)
            self.abort(404)


class CategoryDeleteHandler(BaseHandler):
    def get(self, urlsafe):
        category = Category.get_by_key(urlsafe)
        self.render('category_delete_confirm.html', category=category)

    def post(self, urlsafe):
        try:
            Category.delete_by_key(urlsafe)
            self.redirect('/'.format(urlsafe))
        except Exception as e:
            logging.error(e.message)
            self.abort(404)


# ----------------- Product Handlers -----------------
class ProductCreateHandler(BaseHandler):
    def get(self, urlsafe):
        category = Category.get_by_key(urlsafe)
        self.render('product_new.html', category=category)

    def post(self, urlsafe):
        try:
            name = self.request.get('name')
            price = self.request.get('price')
            description = self.request.get('description')

            product = Product(parent=get_default_key(),
                              name=name,
                              price=float(price),
                              description=description)
            product.put()

            category = Category.get_by_key(urlsafe)
            category.add_products(product)

            self.redirect('/product/{}'.format(product.urlsafe))
        except Exception as e:
            logging.error(e.message)
            self.abort(404)


class ProductRetrieveHandler(BaseHandler):
    def get(self, urlsafe):
        try:
            product = Product.get_by_key(urlsafe)
            self.render('product_detail.html', product=product)
        except Exception as e:
            logging.error(e.message)
            self.abort(404)


class ProductUpdateHandler(BaseHandler):
    def get(self, urlsafe):
        product = Product.get_by_key(urlsafe)
        self.render('product_update.html', product=product)

    def post(self, urlsafe):
        name = self.request.get('name')
        price = self.request.get('price')
        description = self.request.get('description')

        try:
            Product.update_by_key(urlsafe,
                                  name=name,
                                  price=float(price),
                                  description=description)
            self.redirect('/product/{}'.format(urlsafe))
        except Exception as e:
            logging.error(e.message)
            self.abort(404)


class ProductDeleteHandler(BaseHandler):
    def get(self, urlsafe):
        product = Product.get_by_key(urlsafe)
        self.render('product_delete_confirm.html', product=product)

    def post(self, urlsafe):
        try:
            Product.delete_by_key(urlsafe)
            self.redirect('/'.format(urlsafe))
        except Exception as e:
            logging.error(e.message)
            self.abort(404)


app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/categories', MainHandler),

    routes.PathPrefixRoute('/category', [
        webapp2.Route('/new', CategoryCreateHandler),
        webapp2.Route('/<urlsafe:[a-zA-Z0-9-]+>', CategoryRetrieveHandler),
        webapp2.Route('/<urlsafe:[a-zA-Z0-9-]+>/update', CategoryUpdateHandler),
        webapp2.Route('/<urlsafe:[a-zA-Z0-9-]+>/delete', CategoryDeleteHandler),
    ]),

    routes.PathPrefixRoute('/product', [
        webapp2.Route('/<urlsafe:[a-zA-Z0-9-]+>/new', ProductCreateHandler),
        webapp2.Route('/<urlsafe:[a-zA-Z0-9-]+>', ProductRetrieveHandler),
        webapp2.Route('/<urlsafe:[a-zA-Z0-9-]+>/update', ProductUpdateHandler),
        webapp2.Route('/<urlsafe:[a-zA-Z0-9-]+>/delete', ProductDeleteHandler),
    ]),

], debug=True)
