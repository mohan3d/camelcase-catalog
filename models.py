from google.appengine.ext import ndb

DEFAULT_CATALOG_NAME = 'default_catalog'


class BaseKind(ndb.Model):
    """Provides basic functionality like get-by-key, update and delete."""

    @property
    def urlsafe(self):
        """Returns urlsafe from entity's key."""
        return self.key.urlsafe()

    @classmethod
    def get_by_key(cls, urlsafe):
        """Returns entity from urlsafe key."""
        key = ndb.Key(urlsafe=urlsafe)
        return key.get()

    @classmethod
    def update_by_key(cls, urlsafe, **kwargs):
        """Updates entity with valid key-worded properties."""
        obj = cls.get_by_key(urlsafe)
        for k, v in kwargs.items():
            if k in cls._properties:
                setattr(obj, k, v)

        obj.put()

    @classmethod
    def delete_by_key(cls, urlsafe):
        """Delete entity from urlsafe key."""
        key = ndb.Key(urlsafe=urlsafe)
        key.delete()


class Product(BaseKind):
    """Describes product model name, price and description"""
    name = ndb.StringProperty(required=True)
    price = ndb.FloatProperty(required=True)
    description = ndb.TextProperty()

    def __unicode__(self):
        return self.name


class Category(BaseKind):
    """Describes category model name, description and a list of product keys"""
    name = ndb.StringProperty(required=True)
    description = ndb.TextProperty()
    product_keys = ndb.KeyProperty(Product, repeated=True)

    def add_products(self, *products):
        """Adds keys of provided products to product_keys."""
        for product in products:
            self.product_keys.append(product.key)
        self.put()

    @property
    def products(self):
        return (k.get() for k in self.product_keys)

    def __unicode__(self):
        return self.name


def get_default_key(catalog_name=DEFAULT_CATALOG_NAME):
    return ndb.Key('CATALOG', catalog_name)
